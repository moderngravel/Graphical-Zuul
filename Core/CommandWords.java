package Core;
/**
 * @author  Michael Kölling and David J. Barnes
 * @author Roger Veldman
 * @version 2011.08.08
 */

public class CommandWords
{
    // a constant array that holds all valid command words
    private static final String[] validCommands = {
        "go","back","look","eat","talkto","give","get","drop","status","items","quit", "help"
    };

    /**
     * Constructor - initialise the command words.
     */
    public CommandWords()
    {
        // nothing to do at the moment...
    }

    /**
     * Check whether a given String is a valid command word. 
     * @return true if it is, false if it isn't.
     */
    public boolean isCommand(String aString)
    {
        for(int i = 0; i < validCommands.length; i++) {
            if(validCommands[i].equals(aString))
                return true;
        }
        // if we get here, the string was not found in the commands
        return false;
    }

    /**
     * Print all valid commands to System.out.
     * @return 
     */
    public String[] getAllCommands() 
    {
        return validCommands;
    }
}
