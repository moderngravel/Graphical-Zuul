package Core;
/**
 * To play this game, create an instance of this class and call the "play"
 * method.
 * 
 * This main class creates and initialises all the others: it creates all rooms,
 * creates the parser and starts the game. It also evaluates and executes the
 * commands that the parser returns.
 * 
 * @author Roger Veldman
 */

public class Game {
	private Parser parser;
	private Player player;
	private boolean unlockComplete, alive, win, finished;

	// this room must be declared so we can unlock it with the main game loop.
	private Room hall;
	private Room root;

	/**
	 * Sets up rooms, items, npcs, etc. and runs game
	 */
	public void createGame() {
		finished = false;
		win = false;
		alive = true;
		unlockComplete = false;

		// create the rooms
		Room path = new Room("path", "The road stretches seemingly forever into the misty peaks behind you");
		Room waterfall = new Room("waterfall",
				"The edge of a cliff.\nThere is a waterfall splashing down into a river before you.\nMust be 50 feet tall. Coarse dirt paths lead south and west"
						+ "\nThere is a bit of rope fastened to a tree here,\nperhaps for climbing downward?");
		Room foot = new Room("foot","You are at the foot of the waterfall.\nThere is a cabin here next to the river's edge.");
		Room overlook = new Room("olook",
				"You are at an overlook. The path stops at the edge.\nIn the distance there is a bit of smoke rising. Slowly... slowly.");
		Room cabin = new Room("cabin","It's a dusty old cabin. There is stairs descending.");
		Room stairs = new Room("stairs","You're on a staircase in the old cabin.");
		Room corridor = new Room("corridor","You're in a dingy corridor. The stairs lead up.\nthe corridor goes west...");
		Room junction = new Room("junction","You're at an intersection of three halls.");
		Room wineCellar = new Room("wcellar","It's a wine cellar. There's a closet, and giant barrels.");
		Room wineCloset = new Room("wcloset","Boring old closet...");
		Room tunnel = new Room("tunnel","It's a tunnel running north and south.");
		Room mansionCellar = new Room("mcellar","You're in the cellar, however this room is furnished");
		Room backRoom = new Room("mback","It's the back room of a lavish mansion.");
		Room fireplace = new Room("fireplace",
				"It's a great room with a tall, tall ceiling.\nA dull ember is burning at a fireplace.");
		Room kitchen = new Room("kitchen",
				"This is a kitchen. Pots and pans are littered about the counter.\nThe smell of freshly baked bread fills the air.");
		Room deck = new Room("deck","You're on the mansion's deck. The deck has stairs that\nlead down into the woods.");
		hall = new Room("hall",
				"There stands before you a great door.\nOn it is engraved the markings of a Koch Snowflake.\nIt is rumored the door will unlock for a kind soul.\n(one who helps mathematicians).");
		Room endRoom = new Room("win","You Win");
		Room upstairsHall = new Room("mupperhall","You're at the top of the stairs. There are several rooms upstairs.");
		Room bedroom = new Room("bedroom","It's the bedroom of some certainly rich person.");
		Room bath = new Room("bath",
				"This bathroom is perhaps the nicest bathroom you've ever seen.\nA portrait of Lewis Carroll hangs over the toilet.\nThere is a door that leads outside to a balcony.");
		Room balcony = new Room("balcony","You can see the surrounding forest from up here on the balcony.");
		Room office = new Room("office",
				"This is an office (of a mathematician most likely).\nThere is a chalkboard with mathematical equation.\n(My oh my... Multivariable calculus).\nOn the table is many... many... papers stacked.\nThe closet is to the south.");
		Room officeCloset = new Room("closet","Well, this is nice...\nOh my a glowing portal is to the south!");
		Room portal = new Room("portal",
				"You are engulfed in the waves of time and space.\nIt is a blinding white color.\nMight want to hurry along...");
		Room frontDoor = new Room("door","You're at the front door of the house you see to the south.");
		Room diningRoom = new Room("dining",
				"Woohoo. You find a dining room.\nThere is a birthday cake saying \"Happy Birthday Fibinocci!\"\nProbably shouldn't take that...");
		Room woods = new Room("wood1","The woods are very strange and mysterious.");
		Room woods2 = new Room("woods2","The woods are very strange and mysterious.");
		Room woods3 = new Room("woods3","The woods are very strange and mysterious.");
		Room street1 = new Room("street1", "It's a city street running east and west.\nYou're on the east end of it.");
		Room street2 = new Room("street2", "It's a city street running east and west.\nYou're on the center bit of it.");
		Room street3 = new Room("street3", "It's a city street running east and west.\nYou're on the west end of it.");
		Room street4 = new Room("street4", "It's a city street running east and west.\nYou're on the far west end of it.");
		Room park = new Room("park", 
				"You're in a city park.\nThere is a torus shaped sculpture.\nThere is a commemorative statue of L'hopital.");
		Room dirtRoad = new Room("road", "It's a dirt road. Theres a farm house right here.");
		Room farmHouse = new Room("farmhouse", "You're in a quaint farm house.");
		Room farmHouseBasement = new Room("farmhouseb", "You're in the basement of the quaint farm house. There's a glowing portal...");
		Room trainStation = new Room("train", "You're at a train station. The platform is to the west.");
		Room platform = new Room("platform", "You're at the train station platform. There's no sign of a train.");
		Room accessRoom = new Room("access", 
				"You're in the mechanical room for the train station...\nShould you be in here?\nA tunnel continues into the abyss.");
		Room quartzTunnel = new Room("quartz", "The room glows with quartz crystals.");
		Room batGuyCave = new Room("batguy","It's bat-guy's bat-lair!\nWho knew he lived here?!?!");
		Room cave = new Room("cave", "Its boring old cave.");
		Room batGuyCloset = new Room("batguy2", "It's bat-guy's closet.\nBat-guy has a lot of grappling hooks.");

		// make exits
		path.setExit("north", waterfall);
		portal.setExit("north", officeCloset);
		waterfall.setExit("south", path);
		waterfall.setExit("down", foot);
		waterfall.setExit("west", overlook);
		overlook.setExit("east", waterfall);
		foot.setExit("up", waterfall);
		foot.setExit("west", cabin);
		foot.setExit("east", cave);
		cabin.setExit("east", foot);
		cabin.setExit("down", stairs);
		stairs.setExit("up", cabin);
		stairs.setExit("down", corridor);
		corridor.setExit("up", stairs);
		corridor.setExit("west", junction);
		junction.setExit("east", corridor);
		junction.setExit("north", tunnel);
		junction.setExit("west", wineCellar);
		wineCellar.setExit("north", wineCloset);
		wineCellar.setExit("east", junction);
		wineCloset.setExit("south", wineCellar);
		tunnel.setExit("north", mansionCellar);
		tunnel.setExit("south", junction);
		mansionCellar.setExit("south", tunnel);
		mansionCellar.setExit("up", backRoom);
		backRoom.setExit("north", fireplace);
		backRoom.setExit("east", hall);
		backRoom.setExit("down", mansionCellar);
		hall.setExit("north", kitchen);
		hall.setLockedExit("east", endRoom); // LOCKED EXIT. Unlocks upon helping all the mathematicians.
		hall.setExit("west", backRoom);
		hall.setExit("up", upstairsHall);
		kitchen.setExit("south", hall);
		kitchen.setExit("west", fireplace);
		kitchen.setExit("east", diningRoom);
		diningRoom.setExit("west", kitchen);
		fireplace.setExit("east", kitchen);
		fireplace.setExit("north", frontDoor);
		fireplace.setExit("west", deck);
		fireplace.setExit("south", backRoom);
		frontDoor.setExit("south", fireplace);
		frontDoor.setExit("north", street1);
		deck.setExit("east", fireplace);
		deck.setExit("north", woods);
		woods.setExit("south", deck);
		woods.setExit("east", woods2);
		woods2.setExit("east", woods3);
		woods2.setExit("west", woods);
		woods3.setExit("east", street1);
		woods3.setExit("west", woods2);
		upstairsHall.setExit("north", office);
		upstairsHall.setExit("down", hall);
		upstairsHall.setExit("west", bedroom);
		bedroom.setExit("north", bath);
		bedroom.setExit("east", upstairsHall);
		bath.setExit("south", bedroom);
		bath.setExit("west", balcony);
		bath.setExit("east", office);
		balcony.setExit("east", bath);
		office.setExit("east", officeCloset);
		office.setExit("west", bath);
		officeCloset.setExit("south", portal);
		officeCloset.setExit("west", office);
		street1.setExit("south", frontDoor);
		street1.setExit("east", street2);
		street1.setExit("west", woods3);
		street2.setExit("west", street1);
		street2.setExit("east", street3);
		street3.setExit("west", street2);
		street3.setExit("east", street4);
		street3.setExit("north", park);
		street4.setExit("east", dirtRoad);
		street4.setExit("west", street3);
		street4.setExit("south", trainStation);
		trainStation.setExit("west", platform);
		trainStation.setExit("north", street4);
		platform.setExit("east", trainStation);
		platform.setExit("down", accessRoom);
		accessRoom.setExit("up", platform);
		accessRoom.setExit("south", quartzTunnel);
		quartzTunnel.setExit("north", accessRoom);
		quartzTunnel.setExit("south", batGuyCave);
		batGuyCave.setExit("north", quartzTunnel);
		batGuyCave.setExit("south", cave);
		batGuyCave.setExit("west", batGuyCloset);
		batGuyCloset.setExit("east", batGuyCave);
		cave.setExit("north", batGuyCave);
		cave.setExit("west", foot);
		park.setExit("south", street3);
		dirtRoad.setExit("west", street4);
		dirtRoad.setExit("north", farmHouse);
		farmHouse.setExit("south", dirtRoad);
		farmHouse.setExit("down", farmHouseBasement);
		farmHouseBasement.setExit("up", farmHouse);

		root = balcony;
		
		// add people
		overlook.addPerson(new NPC("mandelbrot", "julia_set", "Be careful out there."));
		overlook.addPerson(new NPC("menger", "menger_sponge",
				"Hey thanks! This cube here has infinite surface area\nand zero volume.\nI discovered it!"));
		batGuyCave.addPerson(new NPC("bat-guy", "tesseract",
				"Hey, that's bat-tastic! I can use this to power my bat-teries.\nThey're always running out of juice.\nJust kidding, I'm secretly a mathematician. I'm going to study this abstract cube."));
		woods2.addPerson(new NPC("lowe", "mandelbox",
				"I had just set it down... and it was gone! Thank you so incredibly much."));
		hall.addPerson(new NPC("koch", "snowflake",
				"Thanks. Hey, if you help all of us mathematicians, this door might open."));
		balcony.addPerson(new NPC("cantor", "dust",
				"Ah... right where I left it. This dust is no ordinary dust, you know.\nAll these lengths here add up to 1.5\nKnow your limits."));
		park.addPerson(new NPC("leibniz", "curve", "Thanks man."));
		platform.addPerson(new NPC("minkowski", "question_mark", "Awesome."));

		// quest items
		foot.addItem("julia_set", "Better get this back to Mr. Mandelbrot.\nI wonder why he left it here.", 5);
		waterfall.addItem("menger_sponge",
				"It's a strange cube with square holes like a sponge.\nIt feels almost... weightless.", 0);
		portal.addItem("tesseract", "It's a shimmerey object with new dimension of depth.", 10);
		batGuyCloset.addItem("snowflake", "Ouch! It's infintely sharp... or smooth?\n*sigh*\nThese are the questions.",
				2);
		farmHouseBasement.addItem("mandelbox", "This box is wacky! So many complex shapes.\nIt's rather heavy, too.", 15);
		wineCloset.addItem("dust", "A lump of geometrically ordered dust. 1, 1/3, 1/9, etc...", 1);
		cabin.addItem("curve", "Its definetely pretty odd shaped, but definitely polynomial.", 3);
		officeCloset.addItem("question_mark", "It's an odd curve. Is it a fractal? I can't even tell...", 3);

		// add food items
		kitchen.addFoodItem("quaternion_soup", "It looks tasty enough.", 3);
		overlook.addFoodItem("fractalicious_mushroom", "It looks edible...", 1);
		diningRoom.addFoodItem("slice_of_cake", "1,1,2,3,5,8,13,21", 1);
		kitchen.addFoodItem("julias_soup", "I bet it's delicious", 2);

		// final Instances necessary...
		parser = new Parser();
		player = new Player(path);
		
		path.setDiscovered(true);
	}

	public void welcome() {
		// Give the player an initial welcome
		printLineToConsole();
		printLineToConsole(
				"Hello there, and welcome to Fractal Forest,\na wonderful game involving lots of math-related allusions.\nType 'help' for help.");
		printLineToConsole("\nYou've found yourself in a strange, repetitive forest...");
		printLineToConsole(
				"\nLevel up your strength to hold more weight by eating\n(and restore health in doing so).\nEvery room move takes 4 energy, so don't run out, or it's game over.");
		printLineToConsole(
				"\nGive people items with 'give' command followed by their name.\nTalk to them with 'talkto' followed by the person's name.");
		printLineToConsole();
		look();
	}

	/**
	 * Main play routine. Loops until end of play.
	 */
	public void play(String inputLine) {

		checkEndConditions();

		// Enter the main command loop. Here we repeatedly read commands and
		// execute them until the game is over.

		Command command = parser.getCommand(inputLine);
		finished = processCommand(command);

		// check if player is dead. If so, flag it.
		if (player.getEnergy() <= 0) {
			alive = false;
		}

		// check if player has completed all tasks. If so, unlock door to end room.
		if (NPC.peopleTotal == NPC.peopleSatisfied && !unlockComplete) {
			hall.unlockExit("east");
			unlockComplete = true;
			printLineToConsole(
					"*rumble*\n*rumble*\n*RUMBLE*\n*RUMBLEGRUMBLESTUMBLERUMBLE*\nYou hear the earth tremble.\nHas the mysterious door finally unlocked?");
		}

		// check if player has reached the End Room. If so, flag it.
		if (player.getRoom().getShortDescription().equals("You Win")) {
			win = true;
		}

		checkEndConditions();
	}

	private void checkEndConditions() {
		// if the player has died, then print death message, else print generic message.
		if (!alive) {
			printLineToConsole(
					"\n\nYou ran out of energy and fell over dead. Tough Luck.\n(Tip: use 'status' command to monitor your energy next time?");
			System.exit(0);
		} else if (win && finished) {
			printLineToConsole(
					"You enter a room with riches beyond imagination.\nGold coins with the faces of mathematicians past litter the floor.\nIncidentally,you give 94% to charity.\nThanks for playing.");
			System.exit(0);
		} else if (!win && finished) {
			printLineToConsole("\n\nSorry to see you're leaving. Play again some other time.");
			System.exit(0);
		}
	}

	/**
	 * Given a command, process (that is: execute) the command.
	 * 
	 * @param command
	 *            The command to be processed.
	 * @return true If the command ends the game, false otherwise.
	 */
	private boolean processCommand(Command command) {
		boolean wantToQuit = false;

		if (command.isUnknown()) {
			printLineToConsole("I don't know what you mean...");
			return false;
		}

		String commandWord = command.getCommandWord();
		if (commandWord.equals("help")) {
			printHelp();
		} else if (commandWord.equals("go")) {
			goRoom(command);
		} else if (commandWord.equals("quit")) {
			wantToQuit = quit(command);
		} else if (commandWord.equals("look")) {
			look();
		} else if (commandWord.equals("eat")) {
			eat(command);
		} else if (commandWord.equals("status")) {
			printStatus();
		} else if (commandWord.equals("get")) {
			if (!get(command)) {
				printToConsole("That is not an item.");
			}
		} else if (commandWord.equals("drop")) {
			drop(command);
		} else if (commandWord.equals("items")) {
			printInventory();
		} else if (commandWord.equals("back")) {
			goBack();
		} else if (commandWord.equals("talkto")) {
			talk(command);
		} else if (commandWord.equals("give")) {
			giveItem(command);
		}
		// else command not recognised.
		return wantToQuit;
	}

	// implementations of user commands:
	/**
	 * Print out some help information. Here we print some stupid, cryptic message
	 * and a list of the command words.
	 */
	private void printHelp() {
		printLineToConsole("\nCommands:\n");
		for (String s : parser.showCommands()) {
			printToConsole(s + " ");
		}
	}

	/**
	 * Displays NPC info for the user's room.
	 */
	public void talk(Command command) {
		printLineToConsole(player.getRoom().getTalk(command.getSecondWord()));
	}

	/**
	 * Adds energy to player (if not at max and if item is edible).
	 */
	public void eat(Command command) {
		if ((player.getEnergy() >= player.getMaxEnergy())) {
			printLineToConsole("Already at max energy.");
			return;
		} else {
			if (player.eat(command.getSecondWord())) {
				printLineToConsole("Consumed the item.");
				player.nextStrengthLevel();
				player.addEnergy(100);
				player.removeItem(command.getSecondWord());
				printLineToConsole("Max Weight improved to : " + player.getMaxWeight());
				printLineToConsole("Energy is now: " + player.getEnergy());
				return;
			}
		}
		printLineToConsole("Can't eat that.");
	}

	/**
	 * Takes the player back a room.
	 */
	public void goBack() {
		player.backRoom();
		printLocation();
		printItems();
		printPerson();
	}

	/**
	 * Transfer an item from room to user.
	 */
	public boolean get(Command command) {
		if (command.getSecondWord() == null) {
			return false;
		}
		String itemName = command.getSecondWord().toLowerCase();
		for (String name : player.getRoom().getItemNames()) {
			if (name.equals(itemName)) {

				if (player.addItem(itemName, player.getRoom().getItemByName(itemName))) {
					printLineToConsole("got " + itemName + ".");
					printLineToConsole(player.getRoom().getItemByName(itemName).getDescription());
					player.getRoom().remove(itemName);

					return true; // item has been found. returns to prevent duplication and executing further
							// statements.
				} else {
					printLineToConsole(
							"That item is too heavy to put in the Strength.\nMaybe you should lighten your load.");
				}
			}
		}
		// item is not found. print necessary response
		printLineToConsole("I can't seem to find that.");
		return true;
	}

	/**
	 * Transfer an item from user to room.
	 */
	public void drop(Command command) {
		String itemName = command.getSecondWord().toLowerCase();
		for (String name : player.getItemList()) {
			if (name.equals(itemName)) {
				player.getRoom().addItem(itemName, player.getItem(itemName).getDescription(),
						player.getItem(itemName).getWeight());
				player.removeItem(itemName);
				return; // item has been found. returns to prevent duplication and executing further
						// statements.
			}
		}
		// item is not found. print necessary response
		printLineToConsole("You don't have that.");
	}

	/**
	 * Give a user item to satisfy the NPC.
	 */
	public void giveItem(Command command) {
		if (player.getItemList().size() != 0) {
			for (String itemName : player.getItemList()) {
				if (player.getRoom().attemptGive(command.getSecondWord(), itemName)) {
					player.removeItem(itemName);
					printLineToConsole("Gave " + itemName);
					talk(command);
				}
			}
		} else {
			printLineToConsole("You have no items!");
			return;
		}
		printLineToConsole("...");
	}

	/**
	 * Print the status of the users energy, strength, backpack, and people helped.
	 */
	public void printStatus() {
		printLineToConsole();
		printLineToConsole("Energy: " + player.getEnergy() + "/" + player.getMaxEnergy());
		printLineToConsole("Strength: Level " + player.getStrengthLevel());
		printLineToConsole("Carrying Weight: " + player.getWeight() + "/" + player.getMaxWeight());
		printLineToConsole("Helped: " + NPC.peopleSatisfied + "/" + NPC.peopleTotal + " people.");
	}

	/**
	 * Provide a description of the room.
	 */
	public void look() {
		printLocation();
		printItems();
		printPerson();
	}

	/**
	 * Try to in to one direction. If there is an exit, enter the new room,
	 * otherwise print an error message.
	 */
	private void goRoom(Command command) {
		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know where to go...
			printLineToConsole("Go where?");
			return;
		}

		String direction = command.getSecondWord();

		// Try to leave current room.
		Room nextRoom = player.getRoom().getExit(direction);

		if (nextRoom == null) {
			printLineToConsole("You can't go that way.");
		} else {
			printLineToConsole();
			player.setRoom(nextRoom);
			nextRoom.setDiscovered(true);
			player.addEnergy(-4); // every step takes energy
			printLocation();
			printItems();
			printPerson();
		}

	}

	/**
	 * Print the room's long description.
	 */
	public void printLocation() {
		printLineToConsole(player.getRoom().getLongDescription());
	}

	/**
	 * Print the items in the player's bag.
	 */
	public void printInventory() {
		printLineToConsole("Strength Level: " + player.getStrengthLevel());
		printLineToConsole(player.getItemsAsString());
	}

	/**
	 * Print the items in the room.
	 */
	public void printItems() {
		printLineToConsole();
		if (player.getRoom().getItemNames().size() != 0) {
			boolean isAtLeastOneItem = false;
			printLineToConsole(" Items: ");
			for (String itemName : player.getRoom().getItemNames()) {
				printToConsole(itemName + " ");
				isAtLeastOneItem = true;
			}
			if (!isAtLeastOneItem) {
				printToConsole(" No items in this room.\n");
			} else {
				printLineToConsole();
			}

		}
	}

	/**
	 * Print the people in the room if there are any NPCs.
	 */
	public void printPerson() {
		for (String personName : player.getRoom().getNpcNames()) {
			printLineToConsole(personName + " is in this room.");
		}
	}

	/**
	 * "Quit" was entered. Check the rest of the command to see whether we really
	 * quit the game.
	 * 
	 * @return true, if this command quits the game, false otherwise.
	 */
	private boolean quit(Command command) {
		if (command.hasSecondWord()) {
			printLineToConsole("Quit what?");
			return false;
		} else {
			return true; // signal that we want to quit
		}
	}

	/**
	 * Puts print-line command in one place. (I might want to port this to android
	 * later, all those System.out.println's won't work. So I put it here. Very
	 * simple to change.)
	 */
	protected void printLineToConsole(String print) {
		System.out.println(print);
	}

	/**
	 * Output an empty line.
	 */
	protected void printLineToConsole() {
		System.out.println();
	}

	/**
	 * Puts print (not print-line) command in one place.
	 */
	protected void printToConsole(String print) {
		System.out.print(print);
	}
	
	public Room getRoot() {
		return root;
	}
	
	public Room getPlayerRoom() {
		return player.getRoom();
	}
}
