package GUI;
import java.awt.Color;
/**
 * @author Roger Veldman
 */
import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;

import Core.Room;
import Util.Line;
public class CustomPanel extends JPanel {
	private static int SPACING = 100;
	private HashMap<Room, Point> roomsFound;
	private Set<Line> lines;
	private Point drawOffset;
	private Room currentRoom;

	public CustomPanel(Room root) {
        setBackground(Color.WHITE);
        drawOffset = new Point(0,0);
        roomsFound = new HashMap<Room, Point>();
        System.out.println(" " + roomsFound);
        lines = new HashSet<Line>();
        visitRoom(root, 0, 0, null);
        currentRoom = root;
    }
	
	public void setDrawOffset(Room room, int x, int y) {
		Point roomLoc = roomsFound.get(room);
		System.out.println(roomLoc);
		System.out.println(x +  " " + y);
		drawOffset = new Point((int)(roomLoc.getX()/2.0) - x, (int)(roomLoc.getY()/2.0) - y);
		drawOffset = new Point(-roomLoc.x + x/2, -roomLoc.y+10 + y/2);
		currentRoom = room;
		repaint();
	}

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int offsetX = drawOffset.x;
        int offsetY = drawOffset.y;
        for (Room room : roomsFound.keySet()) {
        	if (room == null) {
        		continue;
        	}
        	if (!room.isDiscovered()) {
        		continue;
        	}
			int x = roomsFound.get(room).x;
			int y = roomsFound.get(room).y;
			g.drawOval(x + offsetX, y + offsetY, 4, 4);
			if (room.equals(currentRoom)) {
				g.setColor(Color.green);
				g.drawString(room.getName() + " (in)", x-4 + offsetX, y + offsetY);
			} else {
				g.drawString(room.getName(), x-4 + offsetX, y + offsetY);
			}
			int numNpc = 0;
			g.setColor(Color.red);
			for (String name : room.getNpcNames()) {
				g.drawString(name, x-4 + offsetX, y + offsetY+numNpc*10+12);
				numNpc ++;
			}
			g.setColor(Color.blue);

			for (String name : room.getItemNames()) {
				g.drawString(name, x-4 + offsetX, y + offsetY+numNpc*10+12);
				numNpc ++;
			}
	        g.setColor(Color.black);

			room.getNpcNames();
		}
        for (Line p : lines) {
			g.drawLine(p.x1 + offsetX+2, p.y1 + offsetY+2, p.x2 + offsetX+2,p.y2 + offsetY+2);
		}
    }
    
    private void visitRoom(Room room, int x, int y, Room lastRoom) {
    	if (roomsFound.containsKey(room)) {
    		return;
    	}
		roomsFound.put(room, new Point(x, y));
		
    	for (String dir : room.getExits().keySet()) {
			Room roomOther = room.getExit(dir);
			int newX = x;
			int newY = y;
			if (dir.equals(("north"))) {
				newX = x;
				newY = y - SPACING;
			} else if (dir.equals("south")) {
				newX = x;
				newY = y + SPACING;
			} else if (dir.equals("east")) {
				newX = x + SPACING;
				newY = y;
			} else if (dir.equals("west")) {
				newX = x - SPACING;
				newY = y;
			} else if (dir.equals("down")) {
				newX = x + SPACING*5;
				newY = y  + SPACING*2;
			} else if (dir.equals("up")) {
				newX = x - SPACING*5;
				newY = y  - SPACING*2;
			}

			visitRoom(roomOther, newX, newY, room);
			Point newPoint = roomsFound.get(roomOther);
			lines.add(new Line(x,y,newPoint.x,newPoint.y));
		}
    }

}
