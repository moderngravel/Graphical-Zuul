package GUI;

import javax.swing.JTextArea;

import Core.Game;

/**
 * Represents an area in the game.
 * 
 * @author Roger Veldman
 */
public class GUIGame extends Game {
	private JTextArea jt;

	public GUIGame(JTextArea textarea) {
		this.jt = textarea;
		this.createGame();
	}

	protected void printLineToConsole(String print) {
		jt.append("\n" + print);
	}

	protected void printLineToConsole() {
		jt.append("\n");
	}

	protected void printToConsole(String print) {
		jt.append(print);
	}
}
